Composer build of Mozilla's PDF.js library.
================================================

[![Latest Stable Version](https://poser.pugx.org/clean-composer-packages/pdf-js/version)](https://packagist.org/packages/clean-composer-packages/pdf-js)
[![Total Downloads](https://poser.pugx.org/clean-composer-packages/pdf-js/downloads)](https://packagist.org/packages/clean-composer-packages/pdf-js)
[![License](https://poser.pugx.org/clean-composer-packages/pdf-js/license)](https://packagist.org/packages/clean-composer-packages/pdf-js)


This is a automatically generated composer package of [Mozilla's PDF.js](http://mozilla.github.io/pdf.js/) library.

Installation
------------

```
php composer.phar require clean-composer-packages/pdf-js
```

License
-------

**clean-composer-packages/pdf-js** is released under the Apache License 2.0. See the [LICENSE](LICENSE) for details.
